<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class BlogController extends Controller
{
    public function getIndex(){

      $post = Post::paginate(5);

      return view('blog.index')->withPosts($post);

    }

    public function getSingle($slug)
    {
        // fetch from the database based on the slug

        $post = Post::where('slug', '=', $slug)->first();


        return view('blog.single')->withPost($post);

    }
}
