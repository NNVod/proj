<?php

namespace App\Http\Controllers;

use App\Post;

class PagesController extends Controller
 {

   # process variable  data or params
   # talk to the model
   # receive from the model
   # complie or precess data from the model if needed
   # pass the data to the correct view

    public function getIndex() {


      $posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
      return view('pages/welcome')->withPosts($posts);
    }


    public function getAbout(){

        return view('pages/About');
}

    public function getContact(){

      return view('pages/contact');
    }



 }
