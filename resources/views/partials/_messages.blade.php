

<!-- the flash message when a post is created -->
  @if (Session::has('succes'))


      <div class="alert alert-success" role="alert">
        <strong>Succes:</strong> {{ Session::get('succes') }}
      </div>


  @endif


  @if (count($errors) > 0)

      <div class="alert alert-danger" role="alert">
        <strong>Errors:</strong>
        @foreach ($errors->all() as $error)

          <li> {{ $error }} </li>

        @endforeach

      </div>
  @endif
